#include <stdio.h>
#include <stdlib.h>
#include <CL/cl.h>
#include <math.h>

#define MAX_SOURCE_SIZE (0x100000)

int main(void) 
{
    int m = 10;
    int n = 10;

    // Load the kernel source code into the array source_str
    FILE *fp;
    char *source_str;
    size_t source_size;

    fp = fopen("mult_table.cl", "r");
    if (!fp) {
        fprintf(stderr, "Failed to load kernel.\n");
        exit(1);
    }
    source_str = (char*)malloc(MAX_SOURCE_SIZE);
    source_size = fread( source_str, 1, MAX_SOURCE_SIZE, fp);
    fclose( fp );

   // Allocate host memory

    int *a = (int*)malloc(sizeof(int)*m);
    int *b = (int*)malloc(sizeof(int)*n);
    int *out = (int*)malloc(sizeof(int)*m*n);

    // Create the two input arrays
    for(int i = 0; i < m; i++) {
        a[i] = i+1;
    }
    for(int i = 0; i < n; i++) {
        b[i] = i+1;
    }

    // Get platform and device information
    cl_platform_id platform_id = NULL;
    cl_device_id device_id = NULL;   
    cl_uint ret_num_devices;
    cl_uint ret_num_platforms;
    cl_int ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
    ret = clGetDeviceIDs( platform_id, CL_DEVICE_TYPE_ALL, 1, 
            &device_id, &ret_num_devices);
    
    // Create an OpenCL context
    cl_context context = clCreateContext( NULL, 1, &device_id, NULL, NULL, &ret);

    // Create a command queue
    cl_command_queue command_queue = clCreateCommandQueue(context, device_id, 0, &ret);

    // Create memory buffers on the device for each array
    cl_mem a_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, m*sizeof(int), a, &ret);
    cl_mem b_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, n*sizeof(int), b, &ret);
    cl_mem out_mem_obj = clCreateBuffer(context, CL_MEM_WRITE_ONLY, m*n*sizeof(int), NULL, &ret);
  
    // Create a program from the kernel source
    cl_program program = clCreateProgramWithSource(context, 1, (const char **)&source_str, NULL, &ret);						
    
    // Build the program
    ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
		
    // Create the OpenCL kernel
    cl_kernel kernel = clCreateKernel(program, "multTable", &ret);
 
    // Set the arguments of the kernel
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&a_mem_obj);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&b_mem_obj);
    clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&out_mem_obj);
    clSetKernelArg(kernel, 3, sizeof(cl_int), (void *)&m);
    clSetKernelArg(kernel, 4, sizeof(cl_int), (void *)&n);


    // Execute the OpenCL kernel
    size_t local_item_size[2] = {5, 2};
    size_t global_item_size[2] = {m, n};

    ret = clEnqueueNDRangeKernel(command_queue, kernel, 2, NULL, global_item_size, local_item_size, 0, NULL, NULL);

    // Read the memory buffer out on the device to the local variable out
    ret = clEnqueueReadBuffer(command_queue, out_mem_obj, CL_TRUE, 0, m*n*sizeof(int), out, 0, NULL, NULL);				
			

    // Display the results to the screen
    for(int i = 0; i < m; i++)
    {
        for(int j = 0; j < n; j++)
        {
            printf("%d * %d = %d\n", a[i], b[j], out[i*n+j]);
        }
        printf("######\n");
    }
    
    // Clean up
    ret = clFlush(command_queue);
    ret = clFinish(command_queue);
    ret = clReleaseKernel(kernel);
    ret = clReleaseProgram(program);
    ret = clReleaseMemObject(a_mem_obj);
    ret = clReleaseMemObject(b_mem_obj);
    ret = clReleaseMemObject(out_mem_obj);
    ret = clReleaseCommandQueue(command_queue);
    ret = clReleaseContext(context);
	
    free(a);
    free(b);
    free(out);

    return 0;
} 
