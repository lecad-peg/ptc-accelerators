# ptc-accelerators #

Code examples for the PRACE Training Course "Introduction to parallel programming: accelerators".

## Compiling, running and profiling CUDA and OpenCL codes

### Setting the environment

On `viz.hpc.fs.uni-lj.si` CUDA module for compiling CUDA and OpenCL codes:

    $ module load CUDA/10.1.243-GCC-8.3.0

or TAU module with Java Runtime Environment enabled for compiling and/or profiling CUDA and OpenCL codes is needed:

    $ module purge
    $ module load tau/2.29.1-CUDA
    $ module load jre

Profiling can be done only on the GPU node, an interactive session on it can be started with:

    $ env --unset=LD_PRELOAD TMOUT=600 srun --time=1:0:0 --partition=gpu --x11 --pty bash -i

### Compiling and running

**CUDA codes** can be compiled and run, e.g., with:

    $ nvcc -o hello_cuda hello.cu
    $ ./hello_cuda

**OpenCL codes** can be compiled and run, e.g., with:

    $ gcc -o hello_CL hello_CL.c -lOpenCL
    $ ./hello_CL

## Running pyCUDA and pyOpenCL scripts

On `viz.hpc.fs.uni-lj.si` a Python virtual environment must be created for running Jupyter notebooks, pyCUDA and pyOpenCL scripts. Follow the instructions below to create, set, load and use the virtual environment.

### Setting virtual environment

For every session in a terminal these modules must be loaded:

    $ module load Python/3.7.4-GCCcore-8.3.0
    $ module load CUDA/10.1.243-GCC-8.3.0

Create virtual environment (venv), e.g., in home directory - just once:

    $ python3 -m venv pygpu
    $ notebook/bin/pip install --upgrade pip jupyterlab mpi4py pycuda pyopencl

Activate venv (for every session in a terminal):

    [bogdanl@viz ~]$ source pygpu/bin/activate
    (pygpu) [bogdanl@viz ~]$ cd ptc-accelerators/notebooks

### Starting Jupyter Notebook in browser

    (pygpu) [bogdanl@viz notebooks]$ jupyter notebook

### Running scripts

Python scripts are run, e.g., with:

    (pygpu) [bogdanl@viz ~]$ python riemann_cuda_double.py

Deactivate venv (after finishing work):

    (pygpu) [bogdanl@viz ~]$ deactivate
    [bogdanl@viz ~]$

## Compiling and running CUDA Fortran codes

### Setting the environment

On `gpu02.hpc.fs.uni-lj.si` the NVIDIA HPC module for compiling CUDA Fortran codes can be loaded with:

    $ module purge
    $ module use /opt/pkg/ITER/modules/all
    $ ml NVHPC/21.2


### Compiling and running

**CUDA Fortran codes** can be compiled and run, e.g., with:

    $ nvfortran -o hello_world hello_world.cuf
    $ ./hello_world

## Installing jupyter notebook on gpu02 login node of the HPCFS cluster

### Loading modules

Open a new Konsole shell and load the modules:

~~~bash
module purge
module use /opt/pkg/ITER/modules/all
module load NVHPC/21.2
module load OpenMPI/4.1.2-GCC-10.2.0
module load Python/3.8.6-GCCcore-10.2.0
~~~

This must be done every time a new shell is opened.

### Creating a Python virtual environment with needed Python packages

~~~bash
python3 -m venv notebook
notebook/bin/pip install --upgrade pip jupyterlab notebook mpi4py
~~~

This is done only once.

N.B. Currenly, there's a problem with installing the newest `pycuda` Python package with NVHPC. To run the PyCUDA (and also PyOpenCL) examples in the notebooks one must use an older version of PyCUDA, i.e., `pycuda==2021.1` and install the packages in this way:

~~~bash
notebook/bin/pip install --upgrade pip jupyterlab notebook pycuda==2021.1 pyopencl
~~~

This is also done only once.

### Activating the virtual environment `notebook` and starting Jupyter Notebook in browser:

~~~bash
source notebook/bin/activate
(notebook) [bogdanl@gpu02 ~]$ cd ptc-accelerators/notebooks
(notebook) [bogdanl@gpu02 notebooks]$ jupyter notebook
~~~

This must be done every time a new shell is opened. In the browser you can navigate through the repository, open the notebooks (with `*.ipynb`) and run (or modify and run) them.

Note, that you must be in the home directory (`~`) to activate the virtual environment 

After quitting Jupyter Notebook in browser, the virtual environment in the shell can be deactivated with:

~~~bash
(notebook) [bogdanl@gpu02 notebooks]$ deactivate
[bogdanl@gpu02 notebooks]$
~~~