__kernel void medianTrapezoid(__global double *a, int n) {
    
    int idx = get_global_id(0);
    double x = (double)idx / (double)n;
 
    if(idx < n)
       a[idx] = (exp(-x * x / 2.0) + exp(-(x + 1 / (double)n) * (x + 1 / (double)n) / 2.0)) / 2.0;
}

__kernel void reducerSum(__global double *a, __global double *out, __local double *r, int n, int block_size)
{
    int idx = get_local_id(0);
    double sum = 0;
    for (int i = idx; i < n; i += block_size)
        sum += a[i];
    r[idx] = sum;
    barrier(CLK_LOCAL_MEM_FENCE);

    for (int size = block_size/2; size>0; size/=2) {
        if (idx<size)
            r[idx] += r[idx+size];
        barrier(CLK_LOCAL_MEM_FENCE);
    }
   
    if (idx == 0)
        *out = r[0];
}
