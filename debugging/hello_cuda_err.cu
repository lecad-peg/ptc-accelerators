#include <stdio.h>
 
#define NUM_BLOCKS 16
#define BLOCK_WIDTH 1

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}
 
__global__ void hello()
{
    printf("Hello world! I'm a thread in block %d\n", blockIdx.x);
}
 
 
int main(int argc,char **argv)
{
    // launch the kernel
    hello<<<NUM_BLOCKS, BLOCK_WIDTH-1>>>();
    gpuErrchk(cudaPeekAtLastError());
    gpuErrchk(cudaDeviceSynchronize());
 
    // force the printf()s to flush
    cudaDeviceSynchronize();
 
    printf("That's all!\n");
 
    return 0;
}
