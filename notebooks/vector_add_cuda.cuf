! CUDA kernel for vector addition. Each thread takes care of one element of out
attributes(global) subroutine vector_add(out, a, b, n)
    integer, value :: n
    real, device :: out(n), a(n), b(n)
    integer :: i

    ! Get global thread ID
    i = (blockidx%x-1)*blockdim%x + threadidx%x

    ! Make sure not to go out of bounds
    if (i <= n) then
        out(i) = a(i) + b(i)
    endif
end subroutine vector_add

program main
    use cudafor

    type(dim3) :: blocksPerGrid, threadsPerBlock
    integer :: i

    ! Size of vectors
    integer :: n = 2020

    ! Host input vectors
    real,dimension(:),allocatable :: h_a
    real,dimension(:),allocatable :: h_b
    ! Host output vector
    real,dimension(:),allocatable :: h_out

    ! Device input vectors
    real,device,dimension(:),allocatable :: d_a
    real,device,dimension(:),allocatable :: d_b
    ! Host output vector
    real,device,dimension(:),allocatable :: d_out

    ! Allocate memory for each vector on host
    allocate(h_a(n))
    allocate(h_b(n))
    allocate(h_out(n))

    ! Allocate memory for each vector on GPU
    allocate(d_a(n))
    allocate(d_b(n))
    allocate(d_out(n))

    ! Initialize content of input vectors
    do i=1,n
        h_a(i) = i / 100.0
        h_b(i) = (n - i) / 100.0
    enddo

    ! Implicit copy of host vectors to device
    d_a = h_a(1:n)
    d_b = h_b(1:n)

    ! Number of threads in each thread block
    threadsPerBlock = dim3(1024,1,1)

    ! Number of thread blocks in grid
    blocksPerGrid = dim3(ceiling(real(n)/real(threadsPerBlock%x)),1,1)

    ! Execute the kernel
    call vector_add<<<blocksPerGrid, threadsPerBlock>>>(d_out, d_a, d_b, n)

    ! Implicit copy of device array to host
    h_out = d_out(1:n)

    ! Verification
    do i = 1, n
        if (mod(i, 101) == 0) then
            print *,  h_a(i), "+", h_b(i), "=", h_out(i)
        endif
    enddo

    ! Release device memory
    deallocate(d_a)
    deallocate(d_b)
    deallocate(d_out)

    ! Release host memory
    deallocate(h_a)
    deallocate(h_b)
    deallocate(h_out)

end program main
