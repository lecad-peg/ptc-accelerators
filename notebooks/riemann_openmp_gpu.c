#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

#define N 100000000

double riemann(int n)
{
  double sum = 0;
  
  #pragma omp target teams distribute parallel for simd map(tofrom: sum) map(to: n) reduction(+:sum)
  for(int i = 0; i < n; ++i)
  {
    double x = (double) i / (double) n;
    sum += (exp(-x * x / 2.0) + exp(-(x + 1 / (double)n) * (x + 1 / (double)n) / 2.0)) / 2.0;
  }

  sum *= (1.0 / sqrt(2.0 * M_PI)) / (double) n;

  return sum;
}

int main(int argc, char** argv){
  double start = omp_get_wtime();
  double sum = riemann(N);

  printf("Riemann sum OpenMP GPU (double precision) for N = %d     : %.17g \n", N, sum);
  printf("Total time: \t %f s\n", omp_get_wtime()-start);
}
