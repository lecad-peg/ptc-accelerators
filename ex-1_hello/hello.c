#include <stdio.h>
 
#define N 16
 
int main(int argc,char **argv)
{
    for(int i = 0; i < N; ++i)
    {
      printf("Hello world! I'm iteration %d\n", i);
    }
 
    printf("That's all!\n");
 
    return 0;
}
