#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <cuda.h>
#include <cuda_runtime.h>

#define N 2020
#define MAX_ERR 1e-6

__global__ void vector_add(double *out, double *a, double *b, int n) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if(i < n)
        out[i] = a[i] + b[i];
}

int main(){
    double *a, *b, *out;
    double *d_a, *d_b, *d_out; 

    // Allocate host memory
    a   = (double*)malloc(sizeof(double) * N);
    b   = (double*)malloc(sizeof(double) * N);
    out = (double*)malloc(sizeof(double) * N);

    // Initialize host arrays
    for(int i = 0; i < N; i++){
        a[i] = i / 100.0;
        b[i] = (N - i) / 100.0;
    }

    // Allocate device memory
    cudaMalloc((void**)&d_a, sizeof(double) * N);
    cudaMalloc((void**)&d_b, sizeof(double) * N);
    cudaMalloc((void**)&d_out, sizeof(double) * N);

    // Transfer data from host to device memory
    cudaMemcpy(d_a, a, sizeof(double) * N, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, sizeof(double) * N, cudaMemcpyHostToDevice);

    // Executing kernel
    int threadsPerBlock = 1024;
    //int blocksPerGrid =(N + threadsPerBlock - 1) / threadsPerBlock;
    int blocksPerGrid = N/threadsPerBlock + (N % threadsPerBlock == 0 ? 0:1);
    vector_add<<<blocksPerGrid, threadsPerBlock>>>(d_out, d_a, d_b, N);
    
    // Transfer data back to host memory
    cudaMemcpy(out, d_out, sizeof(double) * N, cudaMemcpyDeviceToHost);
     
    // Verification
    for(int i = 0; i < N; i++){
        assert(fabs(out[i] - a[i] - b[i]) < MAX_ERR);
        if (i % 101 == 0)
          printf("%.2f + %.2f = %.2f\n", a[i], b[i], out[i]);
    }
    printf("PASSED\n");

    // Deallocate device memory
    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_out);

    // Deallocate host memory
    free(a); 
    free(b); 
    free(out);
}
