#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <time.h>

#define N 2020
#define MAX_ERR 1e-6

int main(){
    double *a, *b, *out;
    double *d_a, *d_b, *d_out; 

    // Allocate host memory
    a   = (double*)malloc(sizeof(double) * N);
    b   = (double*)malloc(sizeof(double) * N);
    out = (double*)malloc(sizeof(double) * N);

    // Initialize host arrays
    for(int i = 0; i < N; i++){
        a[i] = i / 100.0;
        b[i] = (N - i) / 100.0;
    }

    for(int i = 0; i < N; i++){
        out[i] = a[i] + b[i];
    }   

    // Verification
    for(int i = 0; i < N; i++){
        assert(fabs(out[i] - a[i] - b[i]) < MAX_ERR);
        if (i % 101 == 0)
          printf("%.2f + %.2f = %.2f\n", a[i], b[i], out[i]);
    }
    printf("PASSED\n");

    // Deallocate host memory
    free(a); 
    free(b); 
    free(out);
}
