from __future__ import print_function
from __future__ import absolute_import
import pycuda.driver as cuda
import pycuda.autoinit
from pycuda.compiler import SourceModule

import time

import numpy

def iDivUp(a, b):
    return a // b + 1

N = 10000000

def riemannCUDA(n):
    a = numpy.empty([n])

    a = a.astype(numpy.float64)

    a_d = cuda.mem_alloc(a.size * a.dtype.itemsize)

    mod = SourceModule("""
        __global__ void medianTrapezoid(double *a, int n)
        {
          int idx = blockIdx.x * blockDim.x + threadIdx.x;
          double x = (double)idx / (double)n;
 
          if(idx < n)
            a[idx] = (exp(-x * x / 2.0) + exp(-(x + 1 / (double)n) * (x + 1 / (double)n) / 2.0)) / 2.0;
        }
        """)

    func = mod.get_function("medianTrapezoid")
    block_size = 1024
    n_blocks = iDivUp(n, block_size)
    blockDim  = (block_size, 1, 1)
    gridDim   = (n_blocks, 1, 1)
    print("CUDA kernel 'medianTrapezoid' launch with %i blocks of %i threads\n" % (n_blocks, block_size))
    func(a_d, numpy.int32(n), block=blockDim, grid=gridDim)

    cuda.memcpy_dtoh(a, a_d)

    Sum = numpy.sum(a) / numpy.sqrt(2 * numpy.pi) / numpy.float64(n)

    return Sum

dev = pycuda.autoinit.device

dev_name = dev.name()
total_memory = dev.total_memory() / 1024.0 / 1024.0 / 1024.0
threads_per_block = dev.get_attribute(pycuda.driver.device_attribute.MAX_THREADS_PER_BLOCK)
sm_count = dev.get_attribute(pycuda.driver.device_attribute.MULTIPROCESSOR_COUNT)

print("Found GPU '%s' with %.3f GB of global memory, max %i threads per block, and %i multiprocessors\n" % 
       (dev_name, total_memory, threads_per_block, sm_count))

start = time.time()

Sum = riemannCUDA(N)

end = time.time()

time_taken = end - start # in seconds

print("Riemann sum pyCUDA (double precision) for N = %i  : %.17f" % (N, Sum));
print("Total time (measured by CPU)                              : %f s" % time_taken);
