real(8) function riemann(n)
    integer :: i
    real(8), parameter :: PI = 3.14159265358979323846264338327950288419716939937510
    real(8) :: x, fx
    real(8) :: riemannsum  = 0.0

    ! Riemann sum calculation
    do i = 1, n
        x = dble(i) / dble(n)
        fx = (exp(-x * x / 2.0) + exp(-(x + 1.0 / dble(n)) * (x + 1.0 / dble(n)) / 2.0)) / 2.0
        riemannsum = riemannsum + fx
    enddo

    riemann = riemannsum * (1.0 / sqrt(2.0 * PI)) / dble(n)

end function riemann

program main
    integer :: n = 1000000000
    real(8) :: riemannsum
    real :: start, finish

    call cpu_time(start)
        riemannsum = riemann(n)
    call cpu_time(finish)

    print *,"Riemann sum CPU (double precision) for N = ", n, ": ", riemannsum
    print '("Total time (measured by CPU): ",f6.3," s")', finish-start

end program main